<?php

namespace App\Http\Controllers\Admin\UserManagement;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\School;
use App\Models\Role;
use Illuminate\Http\Request;

class UserManagementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::with('school')->latest()->when(request()->q, function($users) {
            $users = $products->where('username', 'like', '%'. request()->q . '%');
        })->paginate(10);

        return view('admin.user_management.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

     $roles = Role::latest()->get();
     $schools = School::where('status','active')->latest()->get();

     return view('admin.user_management.create',compact('roles','schools'));
 }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

       $this->validate($request, [
           'email'          => 'required|unique:users',
           'username'    => 'required',
           'role_id'        => 'required',
           'school_id'         => 'required',
       ]); 

       $user = User::create([
         'email'          => $request->email,
         'username'          => $request->username,
         'password'           => app('hash')->make('password'),
         'role_id'          => $request->role_id,
         'school_id'       => $request->school_id,
     ]);

       if($user){

        return redirect()->route('admin.user.index')->with(['success' => 'Data Berhasil Disimpan!']);

    }else{

        return redirect()->route('admin.user.index')->with(['error' => 'Data Gagal Disimpan!']);

    }

}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
     $roles = Role::latest()->get();
     $schools = School::where('status','active')->latest()->get();

     return view('admin.user_management.edit', compact('user','roles','schools'));
 }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $this->validate($request, [
           'email'          => 'required|unique:users',
           'username'    => 'required',
           'role_id'        => 'required',
           'school_id'         => 'required',
       ]); 

        $user = User::findOrFail($user->id);
        $user->update([
            'email'          => $request->email,
            'username'          => $request->username,
            'role_id'          => $request->role_id,
            'school_id'       => $request->school_id,
        ]);


        if($user){
            return redirect()->route('admin.user.index')->with(['success' => 'Data Berhasil Diupdate!']);
        }else{
            //redirect dengan pesan error
            return redirect()->route('admin.user.index')->with(['error' => 'Data Gagal Diupdate!']);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();

        if($user){
            return response()->json([
                'status' => 'success'
            ]);

        }else{
            return response()->json([
                'status' => 'error'
            ]);


        }

    }
}
