<?php

namespace App\Http\Controllers\Admin\Discussion;

use App\Http\Controllers\Controller;
use App\Models\Question;
use App\Models\School;
use App\Models\User;
use Illuminate\Http\Request;

class DiscusstionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $discussions = Question::with('school','teacher')->latest()->when(request()->q, function($discussions) {
            $discussions = $products->where('name', 'like', '%'. request()->q . '%');
        })->paginate(10);

        return view('admin.discussion.index',compact('discussions'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $schools = School::latest()->get();
      return view('admin.discussion.create',compact('schools'));
  }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = auth()->user();
        $this->validate($request, [
         'name'          => 'required',
         'level'        => 'required',
         'max_scores'         => 'required'
     ]); 

        $discussion = Question::create([
           'name'          => $request->name,
           'level'          => $request->level,
           'link'          => $request->link,
           'notes'          => $request->notes,
           'max_scores'       => $request->max_scores,
           'school_id'       => auth()->user()->hasRole('superadmin') ?  $request->school_id : auth()->user()->school_id,
           'teacher_id'       => auth()->user()->hasRole('superadmin') ?  $request->teacher_id : auth()->user()->id
       ]);

        if($discussion){
            return redirect()->route('admin.discussion.index')->with(['success' => 'Data Berhasil Disimpan!']);

        }else{

            return redirect()->route('admin.discussion.index')->with(['error' => 'Data Gagal Disimpan!']);

        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getTeacherBySchool(Request $request)
    {

        $cities = User::whereRoleIs("teacher")->where('school_id', $request->get('id'))
        ->pluck('username', 'id');

        return response()->json($cities);
    }
}
