<?php

namespace App\Http\Controllers\Admin\Quiz;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Question;
use App\Models\QuestionDetail;
use App\Models\AnswerDetail;
use DB;

class QuizController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $quizs = Question::with('school','teacher')->latest()->when(request()->q, function($quizs) {
            $quizs = $products->where('name', 'like', '%'. request()->q . '%');
        })->paginate(10);


        return view('admin.quiz.index',compact('quizs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $quiestions = Question::latest()->get();
        return view('admin.quiz.create',compact('quiestions'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $answer = $request->get('answer');
        $isAnswer = $request->get('isTrue'); 
        $arrOutput['answer'] = array();


        $user = auth()->user();
        $this->validate($request, [
           'score'          => 'required|numeric',
           'question_id'        => 'required',
           'question'         => 'required',
       ]); 



        DB::beginTransaction();
        try{
           $questionDetail = QuestionDetail::create([
             'name'          => $request->question,
             'score'          => $request->score,
             'question_id'          => (int) $request->question_id                 
         ]);

           for ($i= 0; $i < count($answer) ; $i++) { 
            if(isset($isAnswer[$i])){
                if($i == $isAnswer[$i]){
                  AnswerDetail::create([
                     'name'          => $answer[$i],
                     'is_answer'          => true,
                     'question_detail_id' => (int) $questionDetail->id                 
                 ]);
              }    
          }else{
              AnswerDetail::create([
                 'name'          => $answer[$i],
                 'is_answer'          => false,
                 'question_detail_id'    => (int)$questionDetail->id                 
             ]);
          }


      }

      DB::commit();
      return redirect()->route('admin.quiz.index')->with(['success' => 'Data Berhasil Disimpan!']);

  } catch (\Exception $th) {
    DB::rollBack();
    return $th;
    return redirect()->route('admin.quiz.index')->with(['error' => $th]);

}

}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Question $quiz)
    {
       $questions = Question::latest()->get();
       $question = Question::find($quiz->id)->with('teacher','school','question_detail.answer_detail')->first();
       
       return view('admin.quiz.edit', compact('questions','question'));
   }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function showById($id){
        $where = array('id' => $id);
        $customer = Question::where($where)->first();
        return Response::json($customer);
    }
}
