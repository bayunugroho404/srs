<?php

namespace App\Http\Controllers\Quiz;

use App\Models\Question;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class QuizController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function getQuestionBySchool(Request $request)
    {
        $question = Question::where('school_id', $request->school_id)->with('teacher', 'question_detail', 'question_detail.answer_detail')->limit(10)->paginate(1);
        return response()->json([
            'status' => '200',
            'message' => 'OK',
            'data' => $question,
        ], 200);
    }

    public function submit(Request $request)
    {
        try {
            $answer = new Answer();
            $answer->user_id = Auth::id();
            $answer->quiz = $request->input('quiz');
            $answer->scores = $request->input('scores');
            $answer->save();

            return response()->json([
                'status' => '200',
                'message' => 'OK',
                'data' => $answer,

            ], 200);
        } catch (\Exception $e) {
            return $e;
            return response()->json([
                'status' => 503,
                'message' => 'FAILED',
                'error' => $e
            ], 503);
        }
    }
}
