<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class DatatableController extends Controller
{
    public function user(DataTables $datatable, Request $request)
    {
        $data = User::with('school')->get();
        return $datatable->of($data)
            ->editColumn('status', function ($data) {
                if ($data->status == "active") {
                    return '<span style="width:50%;" class="badge badge-sm bg-gradient-success"><span style="font-size:12px;">Active</span></span>';
                } else {
                    return '<span style="width:50%;" class="badge badge-sm bg-gradient-danger"><span style="font-size:12px;">Not Active</span></span>';
                }
            })
            ->addColumn('action', function ($data) {
                return
                    '<a id="edit-button" href="' . url("user/form/edit", $data->id) . '" class="btn btn-warning btn-xs edit-button"><i class="fa fa-edit"></i></a>
            <button value="' . $data->id . '" data-content="' . url('student') . '" class="btn btn-danger delete-button"><i class="fa fa-trash"></i></button>';
            })
            ->rawColumns(['status', 'action'])
            ->toJson(true);
    }
}
