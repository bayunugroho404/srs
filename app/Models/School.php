<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class School extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'domain',
        'status'
    ];
    public function user()
    {
        return $this->belongsTo(User::class,'id');
    }


    public function question()
    {
        return $this->belongsTo(Question::class,'id');
    }


}
