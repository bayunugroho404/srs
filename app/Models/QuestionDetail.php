<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QuestionDetail extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'score',
        'type',
        'question_id',
    ];


    public function question()
    {
        return $this->belongsTo(Question::class,'id','question_id');
    }

    public function answer_detail()
    {
        return $this->hasMany(AnswerDetail::class, 'question_detail_id', 'id');
    }


}
