<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AnswerDetail extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'is_answer',
        'question_detail_id'
    ];

     public function question_detail()
    {
        return $this->belongsTo(QuestionDetail::class, 'question_detail_id');
    }



}
