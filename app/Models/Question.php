<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'level',
        'link',
        'notes',
        'max_scores',
        'school_id',
        'teacher_id'
    ];

    public function teacher()
    {
        return $this->hasOne(User::class,'id','teacher_id');
    }


    public function school()
    {
        return $this->hasOne(School::class,'id','school_id');
    }

    public function question_detail()
    {
        return $this->hasMany(QuestionDetail::class,'question_id','id');
    }


}
