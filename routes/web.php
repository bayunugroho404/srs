<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'role:superadmin'])->name('dashboard');

Route::post('dependent-dropdown', 'App\Http\Controllers\Admin\Discussion\DiscusstionController@getTeacherBySchool')
    ->name('dependent-dropdown.index');


Route::group(['middleware' => 'auth'], function () {

    /************************* USER *****************************/
    Route::prefix('user')->group(function () {
        Route::post('/get-quiz-by-school', 'Quiz\QuizController@getQuestionBySchool');
        Route::post('/quiz/submit', 'Quiz\QuizController@submit');
    });
    Route::resource('/order', App\Http\Controllers\Admin\OrderController::class, ['as' => 'admin']);


    /************************* ADMIN *****************************/
    Route::prefix('admin')->group(function () {

        /************************* USER MANAGEMENT *****************************/
        Route::resource('/user', App\Http\Controllers\Admin\UserManagement\UserManagementController::class, ['as' => 'admin']);

        /************************* QUIZ *****************************/
        Route::resource('/quiz', App\Http\Controllers\Admin\Quiz\QuizController::class, ['as' => 'admin']);
        Route::get('show/{id}','App\Http\Controllers\Admin\Discussion\DiscusstionController@showById');

        /************************* DISCUSSION *****************************/
        Route::resource('/discussion', App\Http\Controllers\Admin\Discussion\DiscusstionController::class, ['as' => 'admin']);

    });

    /************************* DATATABLE MANAGEMENT *****************************/
    Route::prefix('datatable')->group(function () {
        Route::get('/user', [App\Http\Controllers\DatatableController::class, 'user']);
    });
});





require __DIR__ . '/auth.php';
