<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
            'name' => 'superadmin',
            'display_name' => 'Super Admin', // optional
            'description' => 'Super admin is the monitoring person of this apps', // optional
        ]);
        Role::create([
            'name' => 'admin',
            'display_name' => 'Admin', // optional
            'description' => 'Admin is the monitoring person of school', // optional
        ]);
        Role::create([
            'name' => 'teacher',
            'display_name' => 'Teacher', // optional
            'description' => 'Teacher is the monitoring person of student', // optional
        ]);
        Role::create([
            'name' => 'student',
            'display_name' => 'Student', // optional
            'description' => 'Student is student :D', // optional
        ]);
    }
}
