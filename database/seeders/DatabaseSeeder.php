<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(SchoolSeeder::class);
        $this->call(RoleSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(QuestionSeeder::class);
        $this->call(QuestionDetailSeeder::class);
        $this->call(AnswerSeeder::class);
        $this->call(AnswerDetailSeeder::class);
    }
}
