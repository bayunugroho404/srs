<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\QuestionDetail;

class QuestionDetailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $school = [
            [
                'name' => 'Apakah sibuk hari ini? ',
                'question_id' => 1
            ],
            [
                'name' => 'Apakah sibuk hari ini ? ',
                'question_id' => 1
            ],
            [
                'name' => 'Apakah sibuk hari ini? ',
                'question_id' => 1
            ],
        ];


        foreach ($school as $key => $value) {
            QuestionDetail::create($value);
        }
    }
}
