<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\AnswerDetail;

class AnswerDetailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $school = [
            [
                'name' => 'iya',
                'is_answer' => false,
                'question_detail_id' => 1
            ],
            [
                'name' => 'tidak',
                'is_answer' => true,
                'question_detail_id' => 1

            ],
            [
                'name' => 'iya mungkin',
                'is_answer' => false,
                'question_detail_id' => 1

            ],
            [
                'name' => 'bisa jadi',
                'is_answer' => false,
                'question_detail_id' => 1

            ]

        ];


        foreach ($school as $key => $value) {
            AnswerDetail::create($value);
        }
    }
}
