<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'username' => 'superadmin',
            'email' => 'superadmin@gmail.com',
            'school_id' => 1,
            'password' => app('hash')->make('password')
        ])->attachRole('superadmin');
        User::create([
            'username' => 'admin',
            'email' => 'admin@gmail.com',
            'school_id' => 1,
            'password' => app('hash')->make('password')
        ])->attachRole('admin');
        User::create([
            'username' => 'teacher',
            'email' => 'teacher@gmail.com',
            'school_id' => 1,
            'password' => app('hash')->make('password')
        ])->attachRole('teacher');
        User::create([
            'username' => 'student',
            'email' => 'student@gmail.com',
            'school_id' => 1,
            'password' => app('hash')->make('password')
        ])->attachRole('student');
    }
}
