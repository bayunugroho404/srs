<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\School;

class SchoolSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $school = [
            [
                'name' => 'Harvard1',
                'domain' => 'https:://youtube.com'
            ],
            [
                'name' => 'Harvard2',
                'domain' => 'https:://youtube.com'
            ],
            [
                'name' => 'Harvard3',
                'domain' => 'https:://youtube.com'
            ],
            [
                'name' => 'Harvard14',
                'domain' => 'https:://youtube.com'
            ],
        ];


        foreach ($school as $key => $value) {
            School::create($value);
        }
    }
}
