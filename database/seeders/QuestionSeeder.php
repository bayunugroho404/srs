<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Question;

class QuestionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $school = [
            [
                'name' => 'Question PG',
                'max_scores' => 100,
                'school_id' => 1,
                'teacher_id' => 3

            ],
            [
                'name' => 'Question Essay',
                'max_scores' => 100,
                'school_id' => 1,
                'teacher_id' => 3
            ],
        ];


        foreach ($school as $key => $value) {
            Question::create($value);
        }
    }
}
