<?php

return [
  'env_stub' => '.env',
  'storage_dirs' => [
    'app' => [
      'public' => [],
    ],
    'framework' => [
      'cache' => [],
      'testing' => [],
      'sessions' => [],
      'views' => [],
    ],
    'logs' => [],
  ],
  'domains' => [
    'nftestingcenter.org' => 'nftestingcenter.org',
    'env' => '.env.nftestingcenter.org'
  ],
];
