		@component('layouts.app')


		@section('content')

		@section('section')

		<form action="{{ route('admin.quiz.store') }}" method="POST">
			@csrf
			@if ($errors->any())
			<div class="alert alert-danger" role="alert">
				<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
			@endif
			@if (Session::has('success'))
			<div class="alert alert-success text-center">
				<p>{{ Session::get('success') }}</p>
			</div>
			@endif
			<div class="form-group">
				<label>PEMBAHASAN</label>
				<select name="question_id" class="form-control">
					<option value="">-- PILIH PEMBAHASAN --</option>
					@foreach ($quiestions as $school)
					<option value="{{ $school->id }}">{{ $school->name }} - {{ $school->max_scores }} max scores</option>
					@endforeach
				</select>

				@error('question_id')
				<div class="invalid-feedback" style="display: block">
					{{ $message }}
				</div>
				@enderror
			</div>

			<div class="form-group">
				<label >Score</label>
				<input type="text" class="form-control" name="score" placeholder="Masukan score">
			</div>

			<table class="table table-bordered" id="dynamicAddRemove">
				<thead>
					<tr>
						<th scope="col"><input type="text" name="question" placeholder="masukan pertanyaan" class="form-control" /></th>
						<th scope="col"><center>Jawaban benar?</center></th>
						<th scope="col"><center><button type="button" name="add" id="dynamic-ar" class="btn btn-outline-primary addRow">+ Jawaban</button></center></th>
					</tr>
				</thead>

				<tbody>
					<tr>
					</tr>
				</tbody>
			</table>
			<button type="submit" class="btn btn-outline-success btn-block">Save</button>


		</form>


		<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js"></script>

		<script type="text/javascript">
			var i = 0;
			$("#dynamic-ar").click(function () {
				if(i <6){
					$("#dynamicAddRemove").append(`<tr>
						<td>
						<input type="text" name="answer[${i}]" class="form-control qty" >
						</td>
						<td>
						<input type="checkbox" name="isTrue[${i}]" value ="${i}" class="form-control">
						</td>
						<td>
						<center>
						<button type="button" class="btn btn-outline-danger remove-input-field">Delete</button>
						</center>
						</td>
						</tr>`
						);	
				}

				i++;
			});
			

			$(document).on('click', '.remove-input-field', function () {
							// $(this).parents('tr').remove();
							var l =$('tbody tr').length;
							if(l==1){
								alert('you cant delete last one')
							}else{
								$(this).parents('tr').remove();
							}
						});
					</script>

					@endsection

					@section('scripts')

					@endsection

					@endsection
					@endcomponent