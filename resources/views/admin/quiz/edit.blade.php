		@component('layouts.app')


		@section('content')

		@section('section')

		<form action="{{ route('admin.quiz.store') }}" method="POST">
			@csrf
			@if ($errors->any())
			<div class="alert alert-danger" role="alert">
				<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
			@endif
			@if (Session::has('success'))
			<div class="alert alert-success text-center">
				<p>{{ Session::get('success') }}</p>
			</div>
			@endif
			
			<div class="form-group">
				<label>PEMBAHASAN</label>
				<select name="question_id" class="form-control">
					<option value="">-- PILIH PEMBAHASAN --</option>
					@foreach ($questions as $quiz)
					@if($question->id == $quiz->id)
					<option value="{{ $quiz->id  }}" selected>{{ $quiz->name }}</option>
					@else
					<option value="{{ $quiz->id  }}">{{ $quiz->name }}</option>
					@endif
					@endforeach
				</select>

				@error('question_id')
				<div class="invalid-feedback" style="display: block">
					{{ $message }}
				</div>
				@enderror
			</div>


			<div class="form-group">
				<label>MAX SCORE</label>
				<input type="text" name="score" value="{{ old('max_scores', $quiz->max_scores) }}" placeholder="Masukkan max_scores"
				class="form-control @error('max_scores') is-invalid @enderror">
				@error('max_scores')
				<div class="invalid-feedback" style="display: block">
					{{ $message }}
				</div>
				@enderror
			</div>

			@foreach ($question->question_detail as $key=>$detail)
			<table class="table table-bordered" id="dynamicAddRemove">
				<thead>
					<tr>
						<th scope="col">

							<div class="row">

								<div class="col-1">
									<center>	{{$key +1}}
									</center>	
								</div>
								
								<div class="col">
									<input type="text" name="question" value="{{ old('detail->name', $detail->name) }}" placeholder="masukan pertanyaan" class="form-control @error('detail->name') is-invalid @enderror" readonly />
									@error('max_scores')
									<div class="invalid-feedback" style="display: block">
										{{ $message }}
									</div>
									@enderror

								</div>					
							</div>
						</th>

						<th scope="col"><center>
							Jawaban
						</center></th>
						<th>
							<center>				
								<div class="btn-group" role="group" aria-label="Button group with nested dropdown">
									<button type="button" class="btn btn-danger">Hapus</button>
									<a class="btn btn-info show-edit" href="javascript:void(0)" id="edit-question" data-toggle="modal" data-id="{{ $detail->id }}">Edit</a>
								</div></center>							
							</th>
						</tr>


						@foreach ($detail->answer_detail as $answer)

						<tr>
							<td>
								<div class="row">

									<div class="col">
										<ul>
											<li>				
												<input type="text" value="{{$answer->name}}" name="answer[{{$answer->id}}]" class="form-control qty" ></li>
											</ul>
										</div>					
									</div>
								</td>
								<td>
									<center>
										<input name="isTrue[${i}]" type="checkbox" class="form-check-input" value="" {{ ($answer->is_answer != "0" ? 'checked' : '')}}>

									</center>
								</td>
								<td>							
									<center>
										<a href=""
										class="btn btn-sm btn-info">
										<i class="fa fa-pencil-alt"></i>
									</a>

								</center>
							</td>
						</tr>
						@endforeach

					</thead>

					<tbody>
						<tr>
						</tr>
					</tbody>
				</table>
				@endforeach



			</form>
			<!-- Show customer modal -->
			<div class="modal fade" id="crud-modal-show" aria-hidden="true" >
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title" id="customerCrudModal-show"></h4>
						</div>
						<div class="modal-body">
							<div class="row">
								<div class="col-xs-2 col-sm-2 col-md-2"></div>
								<div class="col-xs-10 col-sm-10 col-md-10 ">
									@if(isset($customer->name))

									<table>
										<tr><td><strong>Name:</strong></td><td>{{$customer->name}}</td></tr>
										<tr><td><strong>Email:</strong></td><td>{{$customer->email}}</td></tr>
										<tr><td><strong>Address:</strong></td><td>{{$customer->address}}</td></tr>
										<tr><td colspan="2" style="text-align: right "><a href="{{ route('customers.index') }}" class="btn btn-danger">OK</a> </td></tr>
									</table>
									@endif
								</div>
							</div>
						</div>
					</div>
				</div>	
			</div>


			<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
			<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js"></script>

			<script type="text/javascript">

				$(document).ready(function() {
					$(".show-edit").on("click",function() {
						var customer_id = $(this).data('id');
						$.get({{url("admin/show/" )}} + customer_id, function (data) {
							$('#customerCrudModal').html("Edit customer");
							$('#btn-update').val("Update");
							$('#btn-save').prop('disabled',false);
							$('#crud-modal').modal('show');
							$('#cust_id').val(data.id);
							$('#name').val(data.name);
							$('#email').val(data.email);
							$('#address').val(data.address);
						})
					});


					var i = 0;
					$("#dynamic-ar").click(function () {
						if(i <6){
							$("#dynamicAddRemove").append(`<tr>
								<td>
								<input type="text" name="answer[${i}]" class="form-control qty" >
								</td>
								<td>
								<input type="checkbox" name="isTrue[${i}]" value ="${i}" class="form-control">
								</td>
								<td>
								<center>
								<button type="button" class="btn btn-outline-danger remove-input-field">Delete</button>
								</center>
								</td>
								</tr>`
								);	
						}

						i++;
					});


					$(document).on('click', '.remove-input-field', function () {
							// $(this).parents('tr').remove();
							var l =$('tbody tr').length;
							if(l==1){
								alert('you cant delete last one')
							}else{
								$(this).parents('tr').remove();
							}
						});
				});
				function category_manage(id, payload) {
					alert(id);
				}
			</script>

			@endsection

			@section('scripts')

			@endsection

			@endsection
			@endcomponent