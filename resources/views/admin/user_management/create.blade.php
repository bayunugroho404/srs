@component('layouts.app')


@section('content')

@section('section')

<form action="{{route('admin.user.store')}}" method="POST">
	@csrf
	@if ($errors->any())
	<div class="alert alert-danger" role="alert">
		<ul>
			@foreach ($errors->all() as $error)
			<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
	@endif
	@if (Session::has('success'))
	<div class="alert alert-success text-center">
		<p>{{ Session::get('success') }}</p>
	</div>
	@endif


	<div class="container-fluid">
		<!-- Page Heading -->
		<div class="row">
			<div class="col-md-12">
				<div class="card border-0 shadow">
					<div class="card-header">
						<h6 class="m-0 font-weight-bold"><i class="fas fa-shopping-bag"></i> TAMBAH USER</h6>
					</div>
					<div class="card-body">
						<form action="{{route('admin.user.store')}}" method="POST" enctype="multipart/form-data">
							@csrf
							<div class="form-group">
								<label>SEKOLAH</label>
								<select name="school_id" class="form-control">
									<option value="">-- PILIH SEKOLAH --</option>
									@foreach ($schools as $school)
									<option value="{{ $school->id }}">{{ $school->name }}</option>
									@endforeach
								</select>

								@error('school_id')
								<div class="invalid-feedback" style="display: block">
									{{ $message }}
								</div>
								@enderror
							</div>


							<div class="form-group">
								<label>ROLE</label>
								<select name="role_id" class="form-control">
									<option value="">-- PILIH ROLE --</option>
									@foreach ($roles as $role)
									<option value="{{ $role->id }}">{{ $role->name }}</option>
									@endforeach
								</select>

								@error('role_id')
								<div class="invalid-feedback" style="display: block">
									{{ $message }}
								</div>
								@enderror
							</div>

							<div class="form-group">
								<label>USERNAME</label>
								<input type="text" name="username" value="{{ old('username') }}" placeholder="Masukkan username"
								class="form-control @error('username') is-invalid @enderror">
								@error('username')
								<div class="invalid-feedback" style="display: block">
									{{ $message }}
								</div>
								@enderror
							</div>

							<div class="form-group">
								<label>EMAIL</label>
								<input type="text" name="email" value="{{ old('email') }}" placeholder="Masukkan email"
								class="form-control @error('email') is-invalid @enderror">
								@error('email')
								<div class="invalid-feedback" style="display: block">
									{{ $message }}
								</div>
								@enderror
							</div>

					
							<div class="badge badge-info">
								password akan terbuat default "password"
							</div>
							<br>

							<br>
							<button class="btn btn-primary mr-1 btn-submit" type="submit"><i class="fa fa-paper-plane"></i>
							SIMPAN</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /.container-fluid -->


	@endsection

	@section('scripts')

	@endsection

	@endsection
	@endcomponent
	--