		@component('layouts.app')


		@section('content')

		@section('section')

		<form action="{{ route('admin.discussion.store') }}" method="POST">
			@csrf
			@if ($errors->any())
			<div class="alert alert-danger" role="alert">
				<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
			@endif
			@if (Session::has('success'))
			<div class="alert alert-success text-center">
				<p>{{ Session::get('success') }}</p>
			</div>
			@endif
			

			@if(auth()->user()->hasRole('superadmin'))
			<div class="form-group">
				<label>SEKOLAH</label>
				<select name="school_id" id="school" class="form-control">
					@foreach ($schools as $school)
					<option value="{{ $school->id }}">{{ $school->name }}</option>
					@endforeach
				</select>

				@error('school_id')
				<div class="invalid-feedback" style="display: block">
					{{ $message }}
				</div>
				@enderror
			</div>


			<div class="form-group">
				<label>GURU</label>
				<select name="teacher_id" id="teacher" class="form-control">
					
				</select>

				@error('teacher_id')
				<div class="invalid-feedback" style="display: block">
					{{ $message }}
				</div>
				@enderror
			</div>

			@endif

			<div class="form-group">
				<label>LEVEL</label>
				<select name="level" class="form-control">
					<option value="1">LEVEL 1 </option>
					<option value="2">LEVEL 2 </option>
					<option value="3">LEVEL 3 </option>
					<option value="4">LEVEL 4 </option>
					<option value="5">LEVEL 5 </option>
				</select>

				@error('level')
				<div class="invalid-feedback" style="display: block">
					{{ $message }}
				</div>
				@enderror
			</div>


			<div class="form-group">
				<label >NAMA PEMBAHASAN</label>
				<input type="text" class="form-control" name="name" placeholder="Masukan Pembahasan">
			</div>


			<div class="form-group">
				<label >MAXIMAL SCORE</label>
				<input type="text" class="form-control" name="max_scores" placeholder="Masukan score">
			</div>

			<div class="form-group">
				<label >LINK</label>
				<input type="text" class="form-control" placeholder="(optional)" name="link" >
			</div>


			<div class="form-group">
				<label >NOTES</label>
				<input type="text" class="form-control" placeholder="(optional)" name="notes" >
			</div>

			
			<button type="submit" class="btn btn-outline-success btn-block">Save</button>


		</form>


		<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js"></script>
		<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
		<script type="text/javascript">
			$(function () {
				$.ajaxSetup({
					headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
				});

				$('#school').on('change', function () {
					axios.post('{{ url("dependent-dropdown") }}', {id: $(this).val()})
					.then(function (response) {
						$('#teacher').empty();

						$.each(response.data, function (id, name) {
							$('#teacher').append(new Option(name, id))
						})
					});
				});
			});
		</script>

		@endsection

		@section('scripts')

		@endsection

		@endsection
		@endcomponent